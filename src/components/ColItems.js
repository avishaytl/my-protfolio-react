import React from 'react'; 
import '../App.css';  
import ReactList from 'react-list'; 
import * as Icons from "react-icons/di";
import  * as IconsFa  from "react-icons/fa"; 
export default class ColItems extends React.Component {   
  constructor(props){
    super(props);
    this.state = { 
        accounts: [
            {icon:<IconsFa.FaLaravel className="game-icon" style={{color: '#31B495'}}/>},
            {icon:<IconsFa.FaReact className="game-icon" style={{color: '#F4D21F'}}/>},
            {icon:<IconsFa.FaReacteurope className="game-icon" style={{color: '#1AACA8'}}/>},
            {icon:<IconsFa.FaJava className="game-icon" style={{color: '#1F8C43'}}/>},
            {icon:<IconsFa.FaAndroid className="game-icon" style={{color: '#1AA5D0'}}/>},
            {icon:<IconsFa.FaHtml5 className="game-icon" style={{color: '#1AA5D0'}}/>},
            {icon:<IconsFa.FaPhp className="game-icon" style={{color: '#1F8C43'}}/>},
            {icon:<IconsFa.FaApple className="game-icon" style={{color: '#9DCA40'}}/>},
            {icon:<Icons.DiJavascript1 className="game-icon" style={{color: '#DA5A98'}}/>},
            {icon:<Icons.DiPhp className="game-icon" style={{color: '#E5683E'}}/>},
            {icon:<IconsFa.FaLaravel className="game-icon" style={{color: '#31B495'}}/>},
            {icon:<IconsFa.FaReact className="game-icon" style={{color: '#F4D21F'}}/>},
            {icon:<IconsFa.FaReacteurope className="game-icon" style={{color: '#1AACA8'}}/>},
            {icon:<IconsFa.FaJava className="game-icon" style={{color: '#1F8C43'}}/>},
            {icon:<IconsFa.FaAndroid className="game-icon" style={{color: '#1AA5D0'}}/>},
            {icon:<IconsFa.FaHtml5 className="game-icon" style={{color: '#1AA5D0'}}/>},
            {icon:<IconsFa.FaPhp className="game-icon" style={{color: '#1F8C43'}}/>},
            {icon:<IconsFa.FaApple className="game-icon" style={{color: '#9DCA40'}}/>},
            {icon:<Icons.DiJavascript1 className="game-icon" style={{color: '#DA5A98'}}/>},
            {icon:<Icons.DiPhp className="game-icon" style={{color: '#E5683E'}}/>},
            {icon:<IconsFa.FaLaravel className="game-icon" style={{color: '#31B495'}}/>},
            {icon:<IconsFa.FaReact className="game-icon" style={{color: '#F4D21F'}}/>},
            {icon:<IconsFa.FaReacteurope className="game-icon" style={{color: '#1AACA8'}}/>},
            {icon:<IconsFa.FaJava className="game-icon" style={{color: '#1F8C43'}}/>},
            {icon:<IconsFa.FaAndroid className="game-icon" style={{color: '#1AA5D0'}}/>},
            {icon:<IconsFa.FaHtml5 className="game-icon" style={{color: '#1AA5D0'}}/>},
            {icon:<IconsFa.FaPhp className="game-icon" style={{color: '#1F8C43'}}/>},
            {icon:<IconsFa.FaApple className="game-icon" style={{color: '#9DCA40'}}/>},
            {icon:<Icons.DiJavascript1 className="game-icon" style={{color: '#DA5A98'}}/>},
            {icon:<Icons.DiPhp className="game-icon" style={{color: '#E5683E'}}/>},
        ],
    } 
    this.renderItem = this.renderItem.bind(this);
  }   
   
  renderItem(index, key) {
    return <div className="col-item" key={key}>
        <div className="col-item-inside">
            {this.state.accounts[index].icon}
        </div>
    </div>;
  }    
  render(){
    return (   
        <div className="game-col" id={this.props.id} style={{position:'relative', top: this.props.top}}> 
                    <ReactList   
                        itemRenderer={this.renderItem}
                        length={this.state.accounts.length}
                        useStaticSize ={true}
                        threshold ={3000}
                    /> 
        </div> 
    );
  }
} 
