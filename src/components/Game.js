import React from 'react'; 
import '../App.css'; 
import anime from 'animejs/lib/anime.es.js';
import ColItems from './ColItems';
import * as IconsMd  from "react-icons/md";
import FadeIn from "react-fade-in"; 
export default class Game extends React.Component {   
  constructor(props){
    super(props);
    this.state = { 
        result: 300,
        addScore: '',
        isWin3Or4: false,
        isBtnActive: true,
        isClickActive: true,
        subRes: 0,
    } 
  }      
  restartGame = () => {  
      this.setState({isWin3Or4: false,isBtnActive: true}); 
      this.setState({result: 300, subRes: 0});
      let temp = Math.floor(Math.random() * Math.floor(28));
      if(!temp)temp = 100;
      else temp *= 100; 
      anime({
        targets: '#col-one', 
        top: temp,
        duration: 3000, 
      });
      anime({
        targets: '#col-two', 
        top: temp, 
        duration: 3000, 
      });
      anime({
        targets: '#col-three', 
        top: temp,
        duration: 3000, 
      });
      anime({
        targets: '#col-four', 
        top: temp,
        duration: 3000, 
      });
  }
  startGame = () => {    
    this.setState({isClickActive: false}); 
    setTimeout(() => {
      this.setState({isClickActive: true}); 
    }, 2000);
    let temp = Math.floor(Math.random() * Math.floor(28));
    if(!temp)temp = 100;
    else temp *= 100;
      anime({
        targets: '#col-one', 
        top: temp,
        duration: 1500, 
      });
      anime({
        targets: '#col-two', 
        top: temp, 
        duration: 1500, 
      });
      anime({
        targets: '#col-three', 
        top: temp,
        duration: 1500, 
      });
      anime({
        targets: '#col-four', 
        top: temp,
        duration: 1500, 
      });
      setTimeout(() => {
        anime({
            targets: '#col-one', 
            top: 1700,
            duration: 2500, 
          });
          anime({
            targets: '#col-two', 
            top: 1700, 
            duration: 2500, 
          });
          anime({
            targets: '#col-three', 
            top: 1700,
            duration: 2500, 
          });
          anime({
            targets: '#col-four', 
            top: 1700,
            duration: 2500, 
          });
      }, 500);
      let temp1 = Math.floor(Math.random() * Math.floor(28));
      let temp2 = Math.floor(Math.random() * Math.floor(28));
      let temp3 = Math.floor(Math.random() * Math.floor(28));
      let temp4 = Math.floor(Math.random() * Math.floor(28));
    //   console.log('temp1',temp1);
    //   console.log('temp2',temp2);
    //   console.log('temp3',temp3);
    //   console.log('temp4',temp4);
      if(!temp1)temp1 = 100;
      else temp1 *= 100;
      if(!temp2)temp2 = 100;
      else temp2 *= 100;
      if(!temp3)temp3 = 100;
      else temp3 *= 100;
      if(!temp4)temp4 = 100;
      else temp4 *= 100; 
    let res = this.checkRes(temp1,temp2,temp3,temp4);
    if(res === -1){ 
      this.setState({subRes: this.state.subRes + 1});
      res = (this.state.subRes + 1) * -1;
      console.log('-1',res);
    }
    console.log('res',res);
    if(res === 3 || res === 4){
        this.setState({isBtnActive: false});  
        setTimeout(() => {
            this.setState({isWin3Or4: false,isBtnActive: true}); 
            this.setState({subRes: 0});
        }, 13000);
        if(res === 3)
          res = 6;
        if(res === 4)
          res = 16;
    }
    setTimeout(() => {
        if(res){
             this.setState({addScore: (res > 0 ? '+' : '') + res * 100}); 
            setTimeout(() => {
                this.setState({addScore: ''});  
            }, 1000);
        }
        if(this.state.result + res * 100 > 0)
            this.setState({result: this.state.result + res * 100}); 
        else{ 
            this.gameover();  
        } 
    }, 2000);  
    console.log('this.state.result',this.state.result + res * 100  );
    if(this.state.result + res * 100 <= 0){ 
        this.gameover();
    }
    setTimeout(() => {
      anime({
        targets: '#col-one', 
        top: temp1,
        duration: 2500, 
      });
      anime({
        targets: '#col-two', 
        top: temp2, 
        duration: 2500, 
      });
      anime({
        targets: '#col-three', 
        top: temp3,
        duration: 2500, 
      });
      anime({
        targets: '#col-four', 
        top: temp4,
        duration: 2500, 
      });  
    }, 1000);
  }
  gameover = () => {  
      this.setState({isBtnActive: false,result: 0}); 
      console.log('game-over');  
  }
  checkRes = (val1,val2,val3,val4) =>{
    if(val1 > 2000) val1 -= 2000;
    if(val2 > 2000) val2 -= 2000;
    if(val3 > 2000) val3 -= 2000;
    if(val4 > 2000) val4 -= 2000;
    if(val1 > 1000) val1 -= 1000;
    if(val2 > 1000) val2 -= 1000;
    if(val3 > 1000) val3 -= 1000;
    if(val4 > 1000) val4 -= 1000;
    console.log(val1,val2,val3,val4);
    if( val1 === val2 && val1 === val3 && val1 === val4 ){
        setTimeout(() => { 
            this.setState({isWin3Or4: true});  
        }, 1000);
        return 4;
    }
    if( (val1 === val2 && val1 === val3) || (val1 === val3 && val1 === val4) || (val1 === val4 && val1 === val2) ){
        setTimeout(() => { 
            this.setState({isWin3Or4: true});  
        }, 1000);
        return 3;
    }
    if( (val2 === val1 && val2 === val3) || (val2 === val4 && val2 === val3) || (val2 === val1 && val2 === val4) ){
        setTimeout(() => { 
            this.setState({isWin3Or4: true});  
        }, 1000);
        return 3;
    }
    if( (val3 === val2 && val3 === val1) || (val3 === val4 && val3 === val1) || (val3 === val1 && val3 === val2) ){
        setTimeout(() => { 
            this.setState({isWin3Or4: true});  
        }, 1000);
        return 3;
    }
    if( (val4 === val2 && val4 === val3) || (val4 === val3 && val4 === val1) || (val4 === val1 && val4 === val2) ){
        setTimeout(() => { 
            this.setState({isWin3Or4: true});  
        }, 1000);
        return 3;
    }
    if( val1 === val2 || val1 === val3 || val1 === val4 )
        return 2;
    if( val2 === val1 || val2 === val3 || val2 === val4 )
        return 2;
    if( val3 === val2 || val3 === val1 || val3 === val4 )
        return 2;
    if( val4 === val2 || val4 === val3 || val4 === val1 )
        return 2;
    return -1; 
  }
  render(){
    return (  
            <div className="game-container">
                <div id="result"><p>Coins:</p><p style={{color:'#ccc373'}}>&nbsp;{this.state.result}&nbsp;</p> {this.state.addScore !== '' ? <FadeIn><p style={{color: this.state.addScore.indexOf('-') === -1 ? 'green' : 'red'}}>{this.state.addScore}</p></FadeIn> : ''}</div>
                <div id="game-over-title" style={{display: this.state.result === 0 ? 'flex' : 'none'}}><p>Game Over!</p></div>
                <div id="box">
                    <div className="content">
                        <div className="game-view">
                            <div style={{position:'absolute',width:'100%',height:110,top:95}}> 
                                <div id="box-win" style={{display: this.state.isWin3Or4 === true ? 'flex' : 'none'}}>
                                    <div style={{position:'absolute',backgroundColor:'rgba(255,255,255,0.3)',color:'white',fontSize:40,zIndex:999999,width:'100%',height:'100%',flexDirection:'column',alignItems:'center',justifyContent:'center',display:'flex'}}>
                                        <h1 style={{margin:0,fontSize:'4vw'}}> 
                                            <span style={{color:"#ffeb3b",fontFamily:'app-MADEW'}}
                                            className="txt-rotate"
                                            data-period="2000"
                                            data-rotate='[ "WOW!!!", "OMG!!!", "LEGEND!!!"]'></span>
                                        </h1> 
                                      </div>
                                    <div className="content-win">
                                    </div>
                                </div>
                            </div>
                            <ColItems id={"col-one"} top={300}/>
                            <ColItems id={"col-two"} top={500}/>
                            <ColItems id={"col-three"} top={1000}/>
                            <ColItems id={"col-four"} top={700}/>
                        </div>
                    </div>
                </div> 
                    <div className="game-dashboard">
                        <div className="game-dashboard-btn"> 
                            <div id="menuBall1" className="menuBall" >
                                <p style={{position:'relative',left:-40,top:8, color: 'white',fontWeight:'bold'}}>Rotate</p>
                                <div className="ball rotate" style={{backgroundColor: this.state.isClickActive === true ? '#ccc373' : '#a22424'}} onClick={()=> this.state.isBtnActive === true && this.state.isClickActive === true ? this.startGame() : null}>
                                    <div className="menuText">
                                        {this.state.isClickActive === true ? <IconsMd.MdLocalPlay style={{fontSize:20}}/> : <IconsMd.MdCropRotate style={{fontSize:20}}/>}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="game-dashboard-btn" style={{backgroundColor:'rgba(0,0,0,0)'}}>
                            <div id="menuBall1" className="menuBall" >
                                <p style={{position:'relative',left:-40,top:8,color:'white',fontWeight:'bold'}}>Restart</p>
                                <div className="ball restart" style={{backgroundColor:'#ccc373'}} onClick={()=>this.restartGame()}>
                                    <div className="menuText">
                                        <IconsMd.MdReplay5 style={{fontSize:20}}/>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
            </div>
    );
  }
} 
