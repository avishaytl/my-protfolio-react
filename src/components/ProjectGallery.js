import React from 'react';  
import '../App.css';
import  * as IconsFa  from "react-icons/fa";  
import imgIphone from '../style/images/iphone.webp'; 
import ReactPlayer from 'react-player';
export default class InputApp extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            gitBtn: 'hidden',
            gitBtnLColor: '#000',
            gitBtnRColor: '#000',
            projects: this.props.projects[0],
        } 
        console.log('projects',this.props.projects[0].videoUrl);
    }
    render(){
        return(
            <div style={{width:'100%',height:'100%',display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'flex-start'}}>
                <div id="projects-right-gallery">
                    <div id="img-iphone-box">
                        <img id="img-iphone" src={imgIphone} alt="iphone"/>
                    </div> 
                    <div style={{width:'50%',height:'100%',position:'relative',top:0,left: '-50%',display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                        <div id="video-iphone-view" style={{width: 95,height:'97%',backgroundColor:'black',borderRadius:15,zIndex:9999999,overflow:'hidden'}}>
                            <ReactPlayer loop={true} volume={0} url={this.state.projects.videoUrl} width={'100%'} height={'100%'}  playing />
                        </div>
                    </div>
                </div>
                <div style={{width:'80%',height:'70%',display:'flex',flexDirection:'column',alignItems:'flex-end',justifyContent:'center',opacity:0.5,position:'absolute',top: 50}}>
                    <div style={{width:'50%',height:'100%',padding:10,paddingTop:40}}>
                        <div style={{width:'90%%',height:'100%',display:'flex',flexDirection:'column',alignItems:'flex-start',justifyContent:'flex-start'}}>
                            <div id="about-projects">
                                <p>{this.state.projects.name}</p>
                                <p>▪ {this.state.projects.info}</p>
                                <p>▪ {this.state.projects.platforms}</p>
                                <p>▪ {this.state.projects.technology}</p>
                            </div>
                            <div id="about-projects-git-btn" style={{opacity:0,visibility: this.props.gitBtn,width:'100%',height:'100%',display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                                <div id="about-btn-git">
                                    <p>GiTHub</p>
                                    <a href={this.state.projects.gitUrl}>{this.state.projects.name}</a>
                                </div>
                                <div style={{width:'90%',height:50,display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                                    <div style={{width:'100%',height:'100%',display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'flex-start'}}>
                                        <IconsFa.FaArrowAltCircleLeft style={{fontSize:'2em',cursor:'pointer',color: this.state.gitBtnLColor}} onMouseOver={()=>this.setState({gitBtnLColor: '#a22424'})} onMouseLeave={()=>this.setState({gitBtnLColor: '#000'})}/>
                                    </div>
                                    <div style={{width:'100%',height:'100%',display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'flex-start'}}>
                                        <IconsFa.FaArrowAltCircleRight style={{fontSize:'2em',cursor:'pointer',color: this.state.gitBtnRColor}} onMouseOver={()=>this.setState({gitBtnRColor: '#a22424'})} onMouseLeave={()=>this.setState({gitBtnRColor: '#000'})}/>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        );
    }
}